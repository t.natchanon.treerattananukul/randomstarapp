//
//  NasaProjUITests.swift
//  NasaProjUITests
//
//  Created by NATCHANON TREERATTANANUKUL on 3/7/2566 BE.
//

import XCTest

final class NasaProjUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
    }
    
    func testTableViewData() {
        app.launch()
        let tableView = app.tables["tableView"]
        XCTAssertTrue(tableView.waitForExistence(timeout: 5.0))
        let cell = tableView.cells.element(matching: .cell, identifier: "NasaCell_0")
        XCTAssertTrue(cell.waitForExistence(timeout: 2.0))
        tableView.swipeUp()
        tableView.swipeDown()
        let nameLabel = cell.staticTexts["NameLabel"]
        XCTAssertTrue(nameLabel.exists)
        let descriptionLabel = cell.staticTexts["DescriptionLabel"]
        XCTAssertTrue(descriptionLabel.exists)
        let dateLabel = cell.staticTexts["DateLabel"]
        XCTAssertTrue(dateLabel.exists)
        let previewImage = cell.images["previewImage"]
        XCTAssertTrue(previewImage.exists)
        let favoriteButton = cell.buttons["favorite"]
        XCTAssertTrue(favoriteButton.exists)
        favoriteButton.tap()
        let tmpName = nameLabel.label
        let tmpDescription = descriptionLabel.label
        let tmpDate = dateLabel.label
        
        // MARK: Navigate to detail screen
        
        cell.tap()
        let detailScreenTitle = app.navigationBars["navigationBar"].staticTexts.element(boundBy: 0)
        XCTAssertTrue(detailScreenTitle.exists)
        XCTAssertEqual(tmpName, detailScreenTitle.label)
        let detailNameLabel = app.staticTexts["NameLabel"]
        XCTAssertTrue(detailNameLabel.exists)
        XCTAssertEqual(tmpName, detailNameLabel.label)
        let detailDescriotionLabel = app.staticTexts["DescriptionLabel"]
        XCTAssertTrue(detailDescriotionLabel.exists)
        XCTAssertEqual(tmpDescription, detailDescriotionLabel.label)
        let detailPreviewImage = app.images["previewImage"]
        XCTAssertTrue(detailPreviewImage.exists)
        let detailCopyright = app.staticTexts["copyrightLabel"]
        XCTAssertTrue(detailCopyright.exists)
        let detailDateLabel = app.staticTexts["dateLabel"]
        XCTAssertTrue(detailDateLabel.exists)
        XCTAssertEqual(tmpDate, detailDateLabel.label)
        let navigationBackButton = app.navigationBars.buttons["Back"]
        navigationBackButton.tap()
        sleep(5)

    }
    
    func testSwitchPage() {
        app.launch()
        let allTabButton = app.buttons["All"]
        XCTAssertTrue(allTabButton.exists)
        allTabButton.tap()
        let favoriteTabButton = app.buttons["Favorite"]
        XCTAssertTrue(favoriteTabButton.exists)
        favoriteTabButton.tap()
    }
    
    func testSortButton () {
        app.launch()
        let sortButton = app.buttons["Sort"]
        XCTAssertTrue(sortButton.exists)
        sortButton.tap()
        let alert = app.alerts["Sort"]
        XCTAssertTrue(alert.waitForExistence(timeout: 3.0))
        let latestAction = alert.buttons["Latest"]
        XCTAssertTrue(latestAction.exists)
        latestAction.tap()
        sortButton.tap()
        let oldestAction = app.buttons["Oldest"]
        XCTAssertTrue(oldestAction.exists)
        oldestAction.tap()

    }
}
