//
//  PageViewController.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 25/6/2566 BE.
//  Copyright (c) 2566 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import Foundation
import UIKit
import Kingfisher
import ProgressHUD

protocol PageDisplayLogic: AnyObject {
    func displayAPODModels(viewModel: [Page.ApodModels.ViewModel.FetchModels])
    func displaySortData(viewModel: Page.SortData.ViewModel)
    func displayFavoriteItem(viewModel: Page.FavoriteModels.ViewModel)
    func showRetryAlert(alerts alert: UIAlertController)
}

class PageViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:  Properties
    
    var pageType: Page.PageType = .all
    var interactor: PageBusinessLogic?
    var router: (NSObjectProtocol & PageRoutingLogic & PageDataPassing)?
    
    // MARK: Lifecycle
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setup()
        if !PageInteractor.fetchDataCalled {
            ProgressHUD.show("loading")
            fetchData()
        }
        setupTable()
    }
    
    // MARK: Setup
    
    private func setupTable() {
        let cellNib = UINib(nibName: "NasaCell", bundle: nil)
        tableView.accessibilityIdentifier = "tableView"
        tableView.register(cellNib, forCellReuseIdentifier: "NasaCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setup() {
        let viewController = self
        let interactor = PageInteractor()
        let presenter = PagePresenter()
        let router = PageRouter()
        let worker = PageWorker()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        interactor.worker = worker
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
}
extension PageViewController: PageDisplayLogic {
    
    // MARK: Fetch Data
    
    func fetchData () {
        let request = Page.ApodModels.Request()
        interactor?.fetchData(request: request)
    }
    
    func displayAPODModels(viewModel: [Page.ApodModels.ViewModel.FetchModels]) {
        PageInteractor.apodModels = viewModel
        ProgressHUD.dismiss()
        self.tableView.reloadData()
    }
    
    func showRetryAlert(alerts alert: UIAlertController) {
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [self] _ in
            let request = Page.ApodModels.Request()
            interactor?.fetchData(request: request)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(retryAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Switch Table
    
    func switchTable() {
        tableView?.reloadData()
    }
    
    // MARK: Sort Options
    
    func sortOptions(by sortingOption: Main.SortingOption) {
        let request = Page.SortData.Request()
        let pageType = self.pageType
        interactor?.sortData(request: request, by: sortingOption, pageType: pageType)
    }
    
    func displaySortData(viewModel: Page.SortData.ViewModel) {
        self.tableView.reloadData()
    }
    
   // MARK: Favorite Function
    
    @objc func markAsFavorite(sender: UIButton) {
        let index = sender.tag
        let request = Page.FavoriteModels.Request()
        let pageType = self.pageType
        let apod = pageType == .all ? PageInteractor.apodModels[index] : PageInteractor.favoriteModels[index]
        interactor?.toggleFavorite(request: request, pageType: pageType, apodModels: apod)
    }
    
    func displayFavoriteItem(viewModel: Page.FavoriteModels.ViewModel) {
        self.tableView.reloadData()
    }
    
}

extension PageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pageType == .all {
            return PageInteractor.apodModels.count
        }
        else if pageType == .favorite {
            return PageInteractor.favoriteModels.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NasaCell", for: indexPath) as? NasaCell
        cell?.accessibilityIdentifier = "NasaCell_\(indexPath.row)"
        let apod = pageType == .all ? PageInteractor.apodModels[indexPath.row] : PageInteractor.favoriteModels[indexPath.row]
        let imageURL = apod.mediaType == "image" ? apod.hdurl : apod.thumbnailUrl
        cell?.nameLabel.text = apod.title
        cell?.dateLabel.text = apod.date
        cell?.descriptionLabel.text = apod.explanation
        cell?.previewImage.kf.setImage(with: URL(string: imageURL!))
        cell?.favButton.tag = indexPath.row
        cell?.favButton.addTarget(self, action: #selector(markAsFavorite(sender:)), for: .touchUpInside)
        let isFavorite = apod.isFavorite! ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        cell?.favButton.setImage(isFavorite, for: .normal)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedAPOD : Page.ApodModels.ViewModel.FetchModels
        if pageType == .all {
            selectedAPOD = PageInteractor.apodModels[indexPath.row]
        }
        else {
            selectedAPOD = PageInteractor.favoriteModels[indexPath.row]
        }
        router?.navigateToDetail(apodModels: selectedAPOD)

        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let apod = pageType == .all ? PageInteractor.apodModels[indexPath.row] : PageInteractor.favoriteModels[indexPath.row]
        let request = Page.FavoriteModels.Request()
        let favoriteActionTitle = apod.isFavorite! ? "Unfavorite" : "Favorite"
        let favoriteAction = UIContextualAction(style: .normal, title: favoriteActionTitle) { [self] (_, _, completion) in
            interactor?.toggleFavorite(request: request, pageType: pageType,apodModels: apod)
            completion(true)
        }
        favoriteAction.backgroundColor = .systemYellow
        let configuration = UISwipeActionsConfiguration(actions: [favoriteAction])
        return configuration
    }
}
