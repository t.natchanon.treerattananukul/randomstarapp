//
//  PageModels.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 25/6/2566 BE.
//  Copyright (c) 2566 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Page
{
    // MARK: Use cases
    enum ApodModels
    {
        struct Request
        {
        }
        struct Response
        {
            var data: [ViewModel.FetchModels]
        }
        struct ViewModel
        {
            struct FetchModels: Decodable {
                let title: String
                let date: String
                let explanation: String
                let hdurl: String?
                let url: String
                let copyright: String?
                let mediaType: String
                let thumbnailUrl: String?
                var isFavorite: Bool?
                
                enum CodingKeys: String, CodingKey {
                    case title
                    case date
                    case explanation
                    case hdurl
                    case url
                    case copyright
                    case mediaType = "media_type"
                    case thumbnailUrl = "thumbnail_url"
                    case isFavorite
                }
            }
        }
    }
    
    enum FavoriteModels {
        struct Request
        {
        }
        struct Response
        {
            var favoriteData: ApodModels.ViewModel.FetchModels
        }
        struct ViewModel
        {
        }
        
    }
    
    enum PageType {
        case all
        case favorite
    }
    
    enum SortData {
        struct Request
        {
        }
        struct Response
        {
        }
        struct ViewModel
        {
        }
        
    }
}
