//
//  PageInteractor.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 25/6/2566 BE.
//  Copyright (c) 2566 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol PageBusinessLogic
{
    func fetchData (request: Page.ApodModels.Request)
    func sortData (request: Page.SortData.Request, by sortingOption: Main.SortingOption, pageType: Page.PageType)
    func toggleFavorite (request: Page.FavoriteModels.Request, pageType: Page.PageType, apodModels: Page.ApodModels.ViewModel.FetchModels)
}

protocol PageDataStore
{
    var data: [Page.ApodModels.ViewModel.FetchModels]? { get }
    static var apodModels: [Page.ApodModels.ViewModel.FetchModels] { get set }
    static var favoriteModels: [Page.ApodModels.ViewModel.FetchModels] { get set }
    static var fetchDataCalled: Bool { get set }
}

class PageInteractor: PageBusinessLogic, PageDataStore {
    static var fetchDataCalled: Bool = false
    static var apodModels: [Page.ApodModels.ViewModel.FetchModels] = []
    static var favoriteModels: [Page.ApodModels.ViewModel.FetchModels] = []
    var data: [Page.ApodModels.ViewModel.FetchModels]?
    var presenter: PagePresentationLogic?
    var worker: PageWorker?
    
    // MARK: Fetch Data
    
    func fetchData (request: Page.ApodModels.Request) {
        worker?.fetchData { [weak self] result in
            switch result {
            case .success(let apod):
                let response = Page.ApodModels.Response(data: apod)
                self!.presenter?.presentApodModels(response: response)
                PageInteractor.fetchDataCalled = true
            case .failure:
                self!.presenter?.fetchIsFail()
                
            }
        }
    }
    
    // MARK: Sort Data
    
    func sortData(request: Page.SortData.Request, by sortingOption: Main.SortingOption, pageType: Page.PageType) {
        print(pageType)
        switch sortingOption {
        case .latest:
            if pageType == .favorite {
                PageInteractor.favoriteModels.sort { $0.date > $1.date }
            } else {
                PageInteractor.apodModels.sort { $0.date > $1.date }
            }
        case .oldest:
            if pageType == .favorite {
                PageInteractor.favoriteModels.sort { $0.date < $1.date }
            } else {
                PageInteractor.apodModels.sort { $0.date < $1.date }
            }
        }
        
        let response = Page.SortData.Response()
        presenter?.presentSortData(response: response)
    }

    
    // MARK: Favorite Function
    
    private func getIndexInFavorites(_ apod: Page.ApodModels.ViewModel.FetchModels) -> Int? {
        return PageInteractor.favoriteModels.firstIndex(where: { $0.date == apod.date })
    }
    
    func toggleFavorite (request: Page.FavoriteModels.Request, pageType: Page.PageType, apodModels: Page.ApodModels.ViewModel.FetchModels) {
        var modifiedApod = apodModels
        modifiedApod.isFavorite?.toggle()
        if !modifiedApod.isFavorite! {
            if let indexToRemove = getIndexInFavorites(modifiedApod) {
                PageInteractor.favoriteModels.remove(at: indexToRemove)
            }
        } else {
            PageInteractor.favoriteModels.append(modifiedApod)
        }
        if let apodIndex = PageInteractor.apodModels.firstIndex(where: { $0.date == modifiedApod.date }) {
            PageInteractor.apodModels[apodIndex] = modifiedApod
        }
        let response = Page.FavoriteModels.Response(favoriteData: modifiedApod)
        presenter?.presentFavoriteItem(response: response)
    }
    
}
