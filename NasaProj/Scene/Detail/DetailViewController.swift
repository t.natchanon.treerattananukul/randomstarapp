//
//  DetailViewController.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 18/6/2566 BE.
//

import Foundation
import UIKit
import Kingfisher

class DetailViewController: UIViewController{
    
    // MARK: IBOutlets
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var copyrightLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var copyrightView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet private weak var radiusView: UIView!
    @IBOutlet private weak var stackDescription: UIStackView!
    @IBOutlet private weak var contentHeight: NSLayoutConstraint!
    @IBOutlet private weak var copyrightViewWidth: NSLayoutConstraint!
    @IBOutlet weak var stackCopyWidth: NSLayoutConstraint!
    @IBOutlet weak var dateViewWidth: NSLayoutConstraint!
    
    // MARK: Properties
    
    var apod: Page.ApodModels.ViewModel.FetchModels?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.accessibilityIdentifier = "navigationBar"
        self.title = apod?.title
        displayUI()
        setupUI()
    }
    
    // MARK: Setup
    
    private func setupUI(){
        dateView.layer.cornerRadius = 20
        copyrightView.layer.cornerRadius = 20
        radiusView.layer.cornerRadius = 40
        radiusView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        descriptionLabel.sizeToFit()
        nameLabel.sizeToFit()
        stackDescription.layoutIfNeeded()
        contentHeight.constant = stackDescription.frame.height + previewImage.frame.height
        stackCopyWidth.constant = copyrightLabel.intrinsicContentSize.width + 10
        dateViewWidth.constant = dateLabel.intrinsicContentSize.width + 10
        copyrightViewWidth.constant = copyrightLabel.intrinsicContentSize.width + 10
    }
    
    private func displayUI() {
        let imageURL = apod?.mediaType == "image" ? apod?.hdurl : apod?.thumbnailUrl
        previewImage.kf.setImage(with: URL(string: imageURL!))
        nameLabel.text = apod?.title
        descriptionLabel.text = apod?.explanation
        dateLabel.text = apod?.date
        if apod?.copyright != nil {
            let cleanCopyright = apod?.copyright!.replacingOccurrences(of: "\n", with: " ")
            copyrightLabel.text = cleanCopyright
        }
    }
}
