//
//  ViewController.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 14/6/2566 BE.
//

import UIKit
import LZViewPager

protocol MainViewProtocol {
    func showSortOptions(viewModels: Main.SortModal.ViewModel)
}

class MainViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var viewPager: LZViewPager!
    
    // MARK:  Properties
    
    private var subControllers: [UIViewController] = []
    var interactor: MainBusinessLogic?
    var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?
    var presenter: MainPresenterProtocol?
    
    // MARK: Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
        {
            super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
            setup()
        }
        
        required init?(coder aDecoder: NSCoder)
        {
            super.init(coder: aDecoder)
            setup()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewPager()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController1")
        vc.title = "All"
        let vc2 = storyboard.instantiateViewController(withIdentifier: "ViewController1")
        vc2.title = "Favorite"
        
        subControllers = [vc, vc2]
        viewPager.reload()
    }
    
    // MARK: Setup
    
    private func setup(){
        let viewController = self
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let router = MainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupViewPager() {
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self
    }
    
    // MARK: Actions
    
    @IBAction func sortButton(_ sender: Any) {
        let request = Main.SortModal.Request()
        interactor?.sortButtonTapped(request: request)
    }
}

extension MainViewController : MainViewProtocol {
    
    // MARK: MainViewProtocol
    
    func showSortOptions(viewModels: Main.SortModal.ViewModel) {
        let currentVC = controller(at: viewPager.currentIndex!) as? PageViewController
        let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
        let latest = UIAlertAction(title: "Latest", style: .default) { _ in
           currentVC?.sortOptions(by: .latest)
        }
        let oldest = UIAlertAction(title: "Oldest", style: .default) { _ in
            currentVC?.sortOptions(by: .oldest)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(latest)
        alert.addAction(oldest)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
}

extension MainViewController: LZViewPagerDelegate, LZViewPagerDataSource {
    func shouldShowIndicator() -> Bool {
        return false
    }
    
    func numberOfItems() -> Int {
        return subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        let viewController = subControllers[index]
        if let pageVC = viewController as? PageViewController {
            pageVC.pageType = index == 0 ? .all : .favorite
            pageVC.switchTable()
        }
        return viewController
    }
    
    func colorForIndicator(at index: Int) -> UIColor{
        return UIColor.black
    }
    
    func heightForHeader() -> CGFloat {
        return 40.0
    }
    
    func shouldEnableSwipeable() -> Bool {
        return false
    }
    
    func button(at index: Int) -> UIButton {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        let topBorder = CALayer()
        topBorder.backgroundColor = #colorLiteral(red: 0.7990846634, green: 0.7990846634, blue: 0.7990846634, alpha: 1)
        topBorder.frame = CGRect(x:0, y: 0, width: view.frame.width/2, height: 1)
        button.layer.addSublayer(topBorder)
        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = #colorLiteral(red: 0.7990846634, green: 0.7990846634, blue: 0.7990846634, alpha: 1)
        bottomBorder.frame = CGRect(x: 0, y: 39, width: view.frame.width/2, height: 1)
        button.layer.addSublayer(bottomBorder)
        return button
    }
    
}
