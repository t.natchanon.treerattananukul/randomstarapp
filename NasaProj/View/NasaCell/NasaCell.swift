//
//  NasaCell.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 14/6/2566 BE.
//

import Foundation
import UIKit

class NasaCell: UITableViewCell{
    
    // MARK: IBOutlet
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        previewImage.layer.cornerRadius = 20
    }
    
}
