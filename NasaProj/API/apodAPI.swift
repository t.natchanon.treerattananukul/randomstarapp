//
//  apodAPI.swift
//  NasaProj
//
//  Created by NATCHANON TREERATTANANUKUL on 22/6/2566 BE.
//

import Foundation
import Alamofire

class APODAPI {
    let APIKey = "5W06orXpYc7ObRVYgxKuTD7fHOuIkAVcb4739i7c"
    let apiURL = "https://api.nasa.gov/planetary/apod?api_key="
    let count = "&count=10"
    let thumbnail = "&thumbs=true"
    
    func getThisDate() -> String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let thisDate = dateFormatter.string(from: currentDate)
        return thisDate
    }
    
    func getPreviousDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let previousDate = Calendar.current.date(byAdding: .day, value: -10, to: Date())
        let tenPreviousDate = dateFormatter.string(from: previousDate!)
        return tenPreviousDate
    }
    
    func fetchAPOD(completion: @escaping (Result<[Page.ApodModels.ViewModel.FetchModels], Error>) -> Void) {
        let startDateEndDate  = "&start_date=\(getPreviousDate())&end_date=\(getThisDate())"
        let request = AF.request(apiURL + APIKey + startDateEndDate + thumbnail)
        request.responseDecodable(of: [Page.ApodModels.ViewModel.FetchModels].self) { response in
            switch response.result {
            case .success(let apod):
                let updatedApod = apod.map { var model = $0; model.isFavorite = false; return model }
                completion(.success(updatedApod))
            case .failure(let error):
                print(error)
                completion(.failure(error))
                
            }
        }
    }
}

