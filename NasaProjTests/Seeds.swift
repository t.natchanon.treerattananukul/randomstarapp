//
//  Seeds.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 4/7/2566 BE.
//

import XCTest
@testable import NasaProj

struct Seeds {
    struct MockData {
        static var apodModels: [Page.ApodModels.ViewModel.FetchModels] {
            return [
                Page.ApodModels.ViewModel.FetchModels(
                    title: "Mock APOD 1",
                    date: "2022-01-01",
                    explanation: "This is a mock APOD explanation 1.",
                    hdurl: "https://example.com/image1_hd.jpg",
                    url: "https://example.com/image1.jpg",
                    copyright: "Mock Copyright",
                    mediaType: "image",
                    thumbnailUrl: "https://example.com/image1_thumbnail.jpg",
                    isFavorite: false
                ),
                Page.ApodModels.ViewModel.FetchModels(
                    title: "Mock APOD 2",
                    date: "2022-01-02",
                    explanation: "This is a mock APOD explanation 2.",
                    hdurl: "https://example.com/image2_hd.jpg",
                    url: "https://example.com/image2.jpg",
                    copyright: nil,
                    mediaType: "image",
                    thumbnailUrl: "https://example.com/image2_thumbnail.jpg",
                    isFavorite: false
                ),
                // Add more mock APOD models as needed
            ]
        }
        
        static var favoriteModels: [Page.ApodModels.ViewModel.FetchModels] {
            return [
                Page.ApodModels.ViewModel.FetchModels(
                    title: "Favorite APOD 1",
                    date: "2022-01-03",
                    explanation: "This is a favorite APOD explanation 1.",
                    hdurl: "https://example.com/favorite1_hd.jpg",
                    url: "https://example.com/favorite1.jpg",
                    copyright: nil,
                    mediaType: "image",
                    thumbnailUrl: "https://example.com/favorite1_thumbnail.jpg",
                    isFavorite: true
                ),
                Page.ApodModels.ViewModel.FetchModels(
                    title: "Favorite APOD 2",
                    date: "2022-01-04",
                    explanation: "This is a favorite APOD explanation 2.",
                    hdurl: "https://example.com/favorite2_hd.jpg",
                    url: "https://example.com/favorite2.jpg",
                    copyright: nil,
                    mediaType: "image",
                    thumbnailUrl: "https://example.com/favorite2_thumbnail.jpg",
                    isFavorite: true
                ),
                // Add more favorite APOD models as needed
            ]
        }
    }
}

