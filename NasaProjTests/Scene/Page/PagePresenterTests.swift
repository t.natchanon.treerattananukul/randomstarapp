//
//  PagePresenterTests.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 4/7/2566 BE.
//

@testable import NasaProj
import XCTest

final class PagePresenterTests: XCTestCase {
    var presenter: PagePresenter!
    var viewControllerSpy: ViewControllerSpy!
    
    override func setUp() {
        super.setUp()
        presenter = PagePresenter()
        viewControllerSpy = ViewControllerSpy()
        presenter.viewController = viewControllerSpy
    }
    
    func testPresentApodModels() {
        // Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        let response = Page.ApodModels.Response(data: apodModels)
        
        // When
        presenter.presentApodModels(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy.displayAPODModelsCalled)
    }
    
    func testPresentSortData() {
        // Given
        let response = Page.SortData.Response()
        
        // When
        presenter.presentSortData(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy.displaySortDataCalled)
    }
    
    func testPresentFavoriteItem() {
        // Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        let index = 0
        let response = Page.FavoriteModels.Response(favoriteData: apodModels[index])
        
        // When
        presenter.presentFavoriteItem(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy.displayFavoriteItemCalled)
    }
    
    func testFetchIsFail() {
        // When
        presenter.fetchIsFail()
        
        // Then
        XCTAssertTrue(viewControllerSpy.showRetryAlertCalled)
    }
    
    class ViewControllerSpy: PageDisplayLogic {
        var displayAPODModelsCalled = false
        var displaySortDataCalled = false
        var displayFavoriteItemCalled = false
        var showRetryAlertCalled = false
        var displayAPODModelsViewModel: [Page.ApodModels.ViewModel.FetchModels]?
        
        func displayAPODModels(viewModel: [Page.ApodModels.ViewModel.FetchModels]) {
            displayAPODModelsCalled = true
            displayAPODModelsViewModel = viewModel
        }
        
        func displaySortData(viewModel: Page.SortData.ViewModel) {
            displaySortDataCalled = true
        }
        
        func displayFavoriteItem(viewModel: Page.FavoriteModels.ViewModel) {
            displayFavoriteItemCalled = true
        }
        
        func showRetryAlert(alerts alert: UIAlertController) {
            showRetryAlertCalled = true
        }
    }
}

