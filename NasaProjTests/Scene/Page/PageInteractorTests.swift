//
//  PageInteractorTests.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 3/7/2566 BE.
//

@testable import NasaProj
import XCTest

final class PageInteractorTests: XCTestCase {
    var interactor: PageInteractor!
    var presenterSpy: PresenterSpy!
    var workerSpy: WorkerSpy!
    
    
    override func setUp() {
        super.setUp()
        presenterSpy = PresenterSpy()
        interactor = PageInteractor()
        workerSpy = WorkerSpy()
        interactor.presenter = presenterSpy
        interactor.worker = workerSpy
        
    }
    
    func testFetchDataSuccess () {
        //Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        workerSpy.fetchDataCompletionResult = .success(apodModels)
        
        // When
        interactor.fetchData(request: Page.ApodModels.Request())
        
        // Then
        XCTAssertTrue(workerSpy.fetchDataCalled)
        XCTAssertTrue(presenterSpy.presentApodModelsCalled)
    }
    
    func testFetchDataFailure() {
        // Given
        let error = NSError(domain: "TestErrorDomain", code: 123, userInfo: nil)
        workerSpy.fetchDataCompletionResult = .failure(error)
        
        // When
        interactor.fetchData(request: Page.ApodModels.Request())
        
        // Then
        XCTAssertTrue(workerSpy.fetchDataCalled)
        XCTAssertTrue(presenterSpy.fetchIsFailCalled)
    }
    
    func testSortDataAllTabLastest () {
        //Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        //When
        interactor.sortData(request: Page.SortData.Request(), by: .latest, pageType: .all)
        
        //Then
        XCTAssertTrue(presenterSpy.presentSortDataCalled)
    }
    
    func testSortDataAllTabOldest () {
        //Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        //When
        interactor.sortData(request: Page.SortData.Request(), by: .oldest, pageType: .all)
        
        //Then
        XCTAssertTrue(presenterSpy.presentSortDataCalled)
    }
    
    func testSortDataFavoriteTabLastest () {
        //Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        //When
        interactor.sortData(request: Page.SortData.Request(), by: .latest, pageType: .favorite)
        
        //Then
        XCTAssertTrue(presenterSpy.presentSortDataCalled)
    }
    
    func testSortDataFavoriteTabOldest () {
        //Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        //When
        interactor.sortData(request: Page.SortData.Request(), by: .oldest, pageType: .favorite)
        
        //Then
        XCTAssertTrue(presenterSpy.presentSortDataCalled)
    }
    
    func testToggleFavorite() {
        // Given
        let apodModels: [Page.ApodModels.ViewModel.FetchModels] = Seeds.MockData.apodModels
        interactor.data = apodModels
        let index = 0
        
        // When
        interactor.toggleFavorite(request: Page.FavoriteModels.Request(), pageType: .all, apodModels: apodModels[index])
            
        // Then
        XCTAssertTrue(presenterSpy.presentFavoriteItemCalled)
        
        let updatedApodModel = presenterSpy.presentFavoriteItemResponse?.favoriteData
        XCTAssertTrue(updatedApodModel?.isFavorite == true, "Favorite status is not toggled correctly")
    }

    
    class PresenterSpy: PagePresentationLogic {
        
        var presentApodModelsCalled = false
        var presentSortDataCalled = false
        var presentFavoriteItemCalled = false
        var presentApodModelsResponse: Page.ApodModels.Response?
        var presentFavoriteItemResponse: Page.FavoriteModels.Response?
        var fetchIsFailCalled = false
        
        func presentApodModels(response: NasaProj.Page.ApodModels.Response) {
            presentApodModelsCalled = true
            presentApodModelsResponse = response
        }
        
        func presentSortData(response: NasaProj.Page.SortData.Response) {
            presentSortDataCalled = true
        }
        
        func presentFavoriteItem(response: NasaProj.Page.FavoriteModels.Response) {
            presentFavoriteItemCalled = true
            presentFavoriteItemResponse = response
        }
        
        func fetchIsFail() {
            fetchIsFailCalled = true
        }
        
        
    }

    class WorkerSpy: PageWorker {
        var fetchDataCalled = false
        var fetchDataCompletionResult: Result<[Page.ApodModels.ViewModel.FetchModels], Error>?

        override func fetchData(completion: @escaping (Result<[Page.ApodModels.ViewModel.FetchModels], Error>) -> Void) {
            if let result = fetchDataCompletionResult {
                completion(result)
                fetchDataCalled = true
            }
        }
    }
}

