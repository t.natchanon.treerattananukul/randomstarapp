//
//  PageRouterTests.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 4/7/2566 BE.
//

@testable import NasaProj
import XCTest

final class PageRouterTests: XCTestCase {
    var router: PageRouter!
    var viewController: PageViewControllerSpy!
    
    override func setUp() {
        super.setUp()
        router = PageRouter()
        viewController = PageViewControllerSpy()
        router.viewController = viewController
    }
    
    func testNavigateToDetail() {
        // Given
        let apodModel = Seeds.MockData.apodModels
        let index = 0
        // When
        router.navigateToDetail(apodModels: apodModel[index])
        
        // Then
        XCTAssertTrue(viewController.showCalled)
        XCTAssertNotNil(viewController.showDestinationViewController)
    }
    
    class PageViewControllerSpy: PageViewController {
        var showCalled = false
        var showDestinationViewController: UIViewController?
        
        override func show(_ viewController: UIViewController, sender: Any?) {
            showCalled = true
            showDestinationViewController = viewController
        }
    }
}


