//
//  MainInteractorTests.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 4/7/2566 BE.
//

@testable import NasaProj
import XCTest

final class MainInteractorTests: XCTestCase {
    var interactor: MainInteractor!
    var presenterSpy: PresenterSpy!
    
    override func setUp() {
        super.setUp()
        interactor = MainInteractor()
        presenterSpy = PresenterSpy()
        interactor.presenter = presenterSpy
    }
    
    func testSortButtonTapped() {
        // Given
        let request = Main.SortModal.Request()
        
        // When
        interactor.sortButtonTapped(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy.sortAlertCalled)
    }
    
    class PresenterSpy: MainPresenterProtocol {
        var sortAlertCalled = false
        var sortAlertResponse: Main.SortModal.Response?
        
        func sortAlert(response: Main.SortModal.Response) {
            sortAlertCalled = true
            sortAlertResponse = response
        }
    }
}

