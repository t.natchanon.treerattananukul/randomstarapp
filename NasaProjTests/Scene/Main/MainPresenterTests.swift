//
//  MainPresenterTests.swift
//  NasaProjTests
//
//  Created by NATCHANON TREERATTANANUKUL on 4/7/2566 BE.
//

@testable import NasaProj
import XCTest

final class MainPresenterTests: XCTestCase {
    var presenter: MainPresenter!
    var viewControllerSpy: ViewControllerSpy!
    
    override func setUp() {
        super.setUp()
        presenter = MainPresenter()
        viewControllerSpy = ViewControllerSpy()
        presenter.viewController = viewControllerSpy
    }
    
    func testSortAlert() {
        // Given
        let response = Main.SortModal.Response()
        
        // When
        presenter.sortAlert(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy.showSortOptionsCalled)
        XCTAssertNotNil(viewControllerSpy.showSortOptionsViewModels)
    }
    
    class ViewControllerSpy: MainViewProtocol {
        var showSortOptionsCalled = false
        var showSortOptionsViewModels: Main.SortModal.ViewModel?
        
        func showSortOptions(viewModels: Main.SortModal.ViewModel) {
            showSortOptionsCalled = true
            showSortOptionsViewModels = viewModels
        }
    }
}
